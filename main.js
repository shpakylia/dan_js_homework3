let num1, num2, operation, number;
let result = 0;

function askInput(index) {
    number =  prompt(`enter ${index} num`, 1);
    if(!number || isNaN(number)){
        askInput(index);
    }
    return number;
}
function askOperation(){
    operation = prompt(`enter operation`, '+');
    if(
    operation !== '-'
    &&
    operation !== '/'
    &&
    operation !== '*'
    &&
    operation !== '+')
        askOperation()
    return operation;
}

function getMathResult(number1, number2, operation) {
    switch (operation) {
        case '+':
            result = +number1 + +number2;
            break;
        case '-':
            result = number1 - number2;
            break;
        case '*':
            result = number1 * number2;
            break;
        case '/':
            result = number1 / number2;
            break;
    }
    return result
}

num1 = askInput(1);
num2 = askInput(2);
operation = askOperation();
getMathResult(num1, num2, operation);


console.log(`${num1} ${operation} ${num2} = ${result}`);